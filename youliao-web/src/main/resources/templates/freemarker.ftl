<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>pdf预览</title>
    <style>
        body {
            font-family: SimHei;
        }
        .pos {
            position: absolute;
            left: 100px;
            top: 150px;
            line-height:20px;
        }
    </style>
</head>
<body>
<div class="blue pos">
    <h1>将进酒</h1>
    <h4>朝代：唐代  作者：${name}</h4>

    <h2>君不见黄河之水天上来，奔流到海不复回。</h2>
    <h2>君不见高堂明镜悲白发，朝如青丝暮成雪。</h2>
    <h2>人生得意须尽欢，莫使金樽空对月。</h2>
    <h2>天生我材必有用，千金散尽还复来。</h2>
    <h2>烹羊宰牛且为乐，会须一饮三百杯。</h2>
    <h2>岑夫子，丹丘生，将进酒，君莫停。</h2>
    <h2>与君歌一曲，请君为我侧耳听。</h2>
    <h2>钟鼓馔玉不足贵，但愿长醉不愿醒。</h2>
    <h2>古来圣贤皆寂寞，惟有饮者留其名。</h2>
    <h2>陈王昔时宴平乐，斗酒十千恣欢谑。</h2>
    <h2>主人何为言少钱，径须沽取对君酌。</h2>
    <h2>五花马，千金裘，呼儿将出换美酒，与尔同销万古愁。</h2>

    <table border="1">
        <tr>
            <th>Month</th>
            <th>Savings</th>
            <th>Who</th>
            <th>Where</th>
        </tr>
        <tr>
            <td>January</td>
            <td>$100</td>
            <td>李白</td>
            <td>成都</td>
        </tr>
    </table>

    <a >这是一个a标签</a>

    <img src="http://tiyi.oss-cn-beijing.aliyuncs.com/test/20190923/fa057a22c4fb4cfda9c5a1d35d5e743b.png"  alt="上海鲜花港 - 郁金香" />

    <h4>一个无序列表：</h4>
    <ul>
        <li>咖啡</li>
        <li>茶</li>
        <li>牛奶</li>
    </ul>
</div>
</body>
</html>